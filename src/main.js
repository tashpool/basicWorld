import { World } from './World/World.js'

async function main() {
  // Get a reference to the container element
  const container = document.querySelector('#scene-container')

  // 1. Create an instance of the World app
  const world = new World(container)

  // 2. Kick off all init tasks
  await world.init()

  // 3. Start the animation loop
  world.start()
}

main().catch((err) => {
  // TODO Use a library to log errors
  console.error(err)
})