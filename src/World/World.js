import { createCamera } from './components/camera.js'
import { createLights } from './components/lights.js'
import { createScene } from './components/scene.js'

import { createControls } from './systems/controls.js'
import { createRenderer } from './systems/renderer.js'
import { Resizer } from './systems/Resizer.js'
import { Loop } from './systems/Loop.js'

// Geometry
import { createCube } from './components/cube.js'

// These variables are module-scoped: we cannot access them
// from outside the module
let camera
let controls
let renderer
let scene
let loop

class World {
  constructor(container) {
    camera = createCamera()
    renderer = createRenderer()
    scene = createScene()
    loop = new Loop(camera, scene, renderer)
    container.append(renderer.domElement)
    controls = createControls(camera, renderer.domElement)

    const { ambientLight, mainLight } = createLights()

    loop.updatables.push(controls)
    scene.add(ambientLight, mainLight)

    const resizer = new Resizer(container, camera, renderer)
  }

  async init() {
    console.log('Initializing world ... ')
    // import models, position, add to scene
    const myCube = createCube()
    scene.add(myCube)
  }

  render() {
    // draw a single frame
    renderer.render(scene, camera)
  }

  start() {
    console.log('Starting the world ...')
    loop.start()
  }

  stop() {
    loop.stop()
  }
}

export { World }