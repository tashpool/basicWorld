import {BoxBufferGeometry, Mesh, MeshPhongMaterial} from 'three'

function createCube() {
  // create a geometry
  const geometry = new BoxBufferGeometry(2, 2, 2)

  // create a default (white) Basic material
  const material = new MeshPhongMaterial({color: 'red'})

  // create a Mesh containing the geometry and material
  return new Mesh(geometry, material)
}

export { createCube }