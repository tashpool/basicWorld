import { Clock } from 'three'

const clock = new Clock()

class Loop {
  constructor(camera, scene, renderer) {
    this.camera = camera
    this.scene = scene
    this.renderer = renderer
    this.updatables = []
  }

  start() {
    this.renderer.setAnimationLoop(() => {
      // ALL animated objects need to update
      this.tick()

      this.renderer.render(this.scene, this.camera)
    })
  }

  stop() {
    this.renderer.setAnimationLoop(null)
  }

  tick() {
    // getDelta once per frame
    const delta = clock.getDelta()
    if (this.updatables.length) {
      for (const object of this.updatables) {
        object.tick(delta)
      }
    }
  }
}

export { Loop }