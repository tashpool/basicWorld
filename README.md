# Three.js Basic Template

Clone and set new origin for usage
```
$ git clone <this project>/basicWorld.git new-world
$ cd new-world
$ git remote remove origin
$ git remote add origin <new repo>/new-world.git
$ git push origin master -u
```

## Webserver
Edit `package.json` scripts with your choice of web server. This example is using parcel as an example.


## Dependencies
* Three.js
* dat.GUI
